
// jumpTargets and jumpSideMenus have matching index
// i.e. if jumpTargets[2] points to "ITEM2", then
//      jumpSideMenus[2] has anchor pointing to "ITEM2"
// Another way would be going through for loops to push elements to jumpSideMenus
var jumpTargets = $(".jump-target");
var jumpSideMenus = $("#side-menu a");
jumpSideMenus.splice(jumpSideMenus.size()-1, 1);


var currentlyActiveMenu;


function isInCurrentViewport(element) {
	// Window (Current Browser Window) Height
	var winHeight = $(window).height();

	// Element's position from top of the page	
	var posFromTop = element.offset().top;

	// Position of the top of the window from the top of the page
	var scrollPos = $(this).scrollTop();

	//console.debug(posFromTop);
	//console.debug(scrollPos);
	//console.debug(scrollPos+winHeight);

	// Use drawing if this "if" condition doesn't make sense
	if (posFromTop >= scrollPos && posFromTop < scrollPos+winHeight) {
		return true;
	} else {
		return false;
	}
};


function elementMostTop(jumpTargetsInViewport) {

	var topElementOffsetFromViewport = $(jumpTargetsInViewport[0]).offset().top - $(this).scrollTop();
	var topElement = $(jumpTargetsInViewport[0]);

	for(i = 1; i < jumpTargetsInViewport.size(); i++) {

		var offsetFromViewport = $(jumpTargetsInViewport[i]).offset().top - $(this).scrollTop();
		//console.debug(offsetFromViewport + " " + topElementOffsetFromViewport);
		if(offsetFromViewport < topElementOffsetFromViewport) {
			topElementOffsetFromViewport = offsetFromViewport;
			topElementIndex = $(jumpTargetsInViewport[i]);
		}
	}

	return topElement;
};



// 1. Filter elements that are in current window call it filteredElements
// 2. If filteredElements' size is 0 then mark all menu inactive
//    else find the topMostElement in filteredElement.
// 3. Make old active menu inactive, and mark topMostElement menu active 
function markMenuActive(jumpTargets) {
	var jumpTargetsInViewport = [];

	for(i = 0; i < jumpTargets.size(); i++) {
		if(isInCurrentViewport($(jumpTargets[i]))) {
			jumpTargetsInViewport.push(jumpTargets[i]);
		}
	}

	//console.debug($(jumpTargetsInViewport));

	if($(jumpTargetsInViewport).size() > 0 ) {
		var topElement = elementMostTop($(jumpTargetsInViewport));
		//console.debug(topElementIndex);
		if(currentlyActiveMenu != undefined) {
			$(currentlyActiveMenu).removeClass("active");
		}

		var topElementIndex;
		for(i = 0; i < jumpTargets.size(); i++) {
			if(topElement[0] == $(jumpTargets)[i]) {
				topElementIndex = i;
				break;
			}
		}

		//console.debug(topElementIndex);

		currentlyActiveMenu = $(jumpSideMenus[topElementIndex]);



		currentlyActiveMenu.addClass("active");
	} else {
		if(currentlyActiveMenu != undefined) {
			$(currentlyActiveMenu).removeClass("active");
		}
	}
}


$(document).ready(function() {
	if($(window).width() < 992) {
		$("#side-menu").hide();
	}
	
	$(".bottom-filler").css("height", $(window).height()*1.5);

    // When page is loaded, check to see if there is any menu to be marked active.
    markMenuActive(jumpTargets);
});

// Execute function everytime scroll is moved
$(window).scroll(function(){
	// $(this).scrollTop() returns distance between top of the screen to current
	// window's top position of scroll. 380-$(this).scrollTop() will eventually go
	// really small so that top position of side menu is 50 when is scrolled down.
    $("#side-menu").css("top",Math.max(50, 380-$(this).scrollTop()));


    // Dynamically Mark Active Menu when Scrolling
    markMenuActive(jumpTargets);


});

$( window ).resize(function() {
	if($(window).width() < 992) {
		$("#side-menu").hide();
	} else {
		$("#side-menu").show();
	}
});
