class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :is_homepage

  def is_homepage(controller, action)
  	if (controller == "sites" && action == "home")
  		true
  	else
  		false
  	end
  end

end
