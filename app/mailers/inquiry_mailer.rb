class InquiryMailer < ActionMailer::Base

  	default from: "website.test.dev@gmail.com"

	def sendInquiry(inquiry)
		@inquiry = inquiry
		mail(to: "website.test.dev@gmail.com", subject: @inquiry.subject)
	end

end
