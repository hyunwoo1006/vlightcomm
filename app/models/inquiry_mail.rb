class InquiryMail < ActiveRecord::Base
	EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,10}\Z/i

	validates_presence_of :email
	validates_length_of :email, :maximum => 150
	validates_format_of :email, :with => EMAIL_REGEX, 
						:message => "is in an incorrect format"
	validates_presence_of :subject
	validates_length_of :subject, :maximum => 200
	validates_presence_of :content
end
